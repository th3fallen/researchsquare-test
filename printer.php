<?php
/**
 * @author Clark Tomlinson  <fallen013@gmail.com>
 * @since 9/10/14, 10:54 AM
 * @link http:/www.clarkt.com
 * @copyright Clark Tomlinson © 2014
 *
 */

/*
 * Please write a program that prints the numbers from 1 to 100.
 * But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”.
 * For numbers which are multiples of both three and five print “FizzBuzz”.
 *
 *
 * To run tests please run vendor/bin/codecept run unit
 */

use CodeTest\Generator;

require_once 'vendor/autoload.php';

$generator = new Generator();

$numbers = $generator->generateNumbers();

echo '<pre>';
print_r($numbers);
echo '</pre>';