<?php
/**
 * @author Clark Tomlinson  <fallen013@gmail.com>
 * @since 9/10/14, 11:05 AM
 * @link http:/www.clarkt.com
 * @copyright Clark Tomlinson © 2014
 *
 */

namespace CodeTest;

class Generator
{
    /**
     * Generates an array of all numbers and replacements
     *
     * @return array
     */
    public function generateNumbers()
    {
        $numbers = [];
        for ($i = 1; $i <= 100; $i ++) {
            if ($i % 3 == 0 && $i % 5 == 0) {
                $numbers[] = 'FizzBuzz';
            } elseif ($i % 3 == 0) {
                $numbers[] = 'Fizz';
            } elseif ($i % 5 == 0) {
                $numbers[] = 'Buzz';
            } else {
                $numbers[] = $i;
            }
        }
        return $numbers;
    }

} 