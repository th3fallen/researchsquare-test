<?php

use Codeception\TestCase\Test;
use CodeTest\Generator;

class GeneratorAcceptanceTest extends Test
{
   /**
    * @var \UnitTester
    */
    protected $tester;

    /**
     * @var Generator
     */
    protected $generator;

    protected $numbers;

    protected function _before()
    {
        $this->generator = new Generator();
        $this->numbers = $this->generator->generateNumbers();

    }

    protected function _after()
    {
    }

    /**
     * Verify that divisors of 3 have been replaced
     */
    public function testOddNumbersAreReplaced()
    {
        $this->assertTrue($this->numbers[2] == 'Fizz');
    }

    /**
     *
     * Verify that divisors of 5 are replaced
     *
     */
    public function testEvenNumbersAreReplaced()
    {
        $this->assertTrue($this->numbers[4] == 'Buzz');
    }

    /**
     * verify that divisors of both are replaced
     */
    public function testThatMultiplesOfBothAreReplaced()
    {
        $this->assertTrue($this->numbers[14] == 'FizzBuzz');
    }

}